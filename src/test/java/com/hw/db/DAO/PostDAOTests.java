package com.hw.db.DAO;

import java.sql.Timestamp;
import java.util.Optional;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hw.db.models.Post;
import com.hw.db.DAO.PostDAO;

public class PostDAOTests {
    JdbcTemplate jdbcMock;
    PostDAO postMock;
    Post post;
    static final Integer postId = 1;
    static final Integer parent = 1;
    static final Integer thread = 1;
    static final String forum = "forum";
    static final Boolean isEdited = false;

    static final String author1 = "author";
    static final String message1 = "message";
    static final Timestamp timing1 = new Timestamp(123);

    static final String author2 = "author2";
    static final String message2 = "message2";
    static final Timestamp timing2 = new Timestamp(124);

    @BeforeEach
    void init() {
        jdbcMock = Mockito.mock(JdbcTemplate.class);
        postMock = new PostDAO(jdbcMock);
        post = new Post();
        post.setAuthor(author1);
        post.setMessage(message1);
        post.setCreated(timing1);

        Mockito.when(
                jdbcMock.queryForObject(
                        Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                        Mockito.any(PostDAO.PostMapper.class),
                        Mockito.eq(postId)))
        .thenReturn(post);
    }

    private static List<Arguments> args() {
        return List.of(
                Arguments.of(
                        postId, author2, message1, timing1,
                        "UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"
                ),
                Arguments.of(
                        postId, author1, message2, timing1,
                        "UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"
                ),
                Arguments.of(
                        postId, author1, message1, timing2,
                        "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"
                ),
                Arguments.of(
                        postId, author2, message2, timing1,
                        "UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"
                ),
                Arguments.of(
                        postId, author2, message1, timing2,
                        "UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"
                ),
                Arguments.of(
                        postId, author1, message2, timing2,
                        "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"
                ),
                Arguments.of(
                        postId, author2, message2, timing2,
                        "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true " +
                        "WHERE id=?;"
                )
        );
    }

    @ParameterizedTest
    @MethodSource("args")
    void testPost(Integer id, String author, String message, Timestamp timing, String query) {
        PostDAO.setPost(id, new Post(author, timing, forum, message, parent, thread, isEdited));
        Mockito.verify(jdbcMock).update(
                Mockito.eq(query),
                Optional.ofNullable(Mockito.any())
        );
    }

    @Test
    void testEmptyPost(){
        PostDAO.setPost(postId, post);
        Mockito.verify(jdbcMock, Mockito.never()).update(Mockito.anyString(), Optional.ofNullable(Mockito.any()));
    }
}