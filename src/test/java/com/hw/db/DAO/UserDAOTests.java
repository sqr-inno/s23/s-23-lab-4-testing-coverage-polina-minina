package com.hw.db.DAO;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;

class UserDAOTests {
    JdbcTemplate jdbcMock;
    UserDAO userMock;

    @BeforeEach
    void init() {
        jdbcMock = Mockito.mock(JdbcTemplate.class);
        userMock = new UserDAO(jdbcMock);
    }

    private static List<Arguments> args() {
        return List.of(
                Arguments.of(
                        "nick", "email", null, null,
                        "UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"
                ),
                Arguments.of(
                        "nick", null, "name", null,
                        "UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"
                ),
                Arguments.of(
                        "nick", null, null, "info_about",
                        "UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"
                ),
                Arguments.of(
                        "nick", "email", "name", null,
                        "UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"
                ),
                Arguments.of(
                        "nick", "email", null, "info_about",
                        "UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"
                ),
                Arguments.of(
                        "nick", null, "name", "info_about",
                        "UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"
                ),
                Arguments.of(
                        "nick", "email", "name", "info_about",
                        "UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"
                )
        );
    }

    @ParameterizedTest
    @MethodSource("args")
    void testChanges(String nick, String email, String name, String info, String query) {
        UserDAO.Change(new User(nick, email, name, info));
        Mockito.verify(jdbcMock).update(Mockito.eq(query), Optional.ofNullable(Mockito.any()));
    }

    @Test
    void testEmptyChanges(){
        UserDAO.Change(new User("nick", null, null, null));
        Mockito.verify(jdbcMock, Mockito.never()).update(Mockito.anyString(), Optional.ofNullable(Mockito.any()));
    }
}