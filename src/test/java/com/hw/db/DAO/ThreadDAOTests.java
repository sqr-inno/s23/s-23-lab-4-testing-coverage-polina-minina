package com.hw.db.DAO;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;

public class ThreadDAOTests {
    JdbcTemplate jdbcMock;
    ThreadDAO threadMock;

    @BeforeEach
    void init() {
        jdbcMock = Mockito.mock(JdbcTemplate.class);
        threadMock = new ThreadDAO(jdbcMock);
    }

    private static List<Arguments> args() {
        return List.of(
                Arguments.of(1, 2, 3, true,
                        "SELECT * FROM \"posts\" WHERE thread = ?  AND " +
                        "branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"
                ),
                Arguments.of(1, 2, 3, false,
                        "SELECT * FROM \"posts\" WHERE thread = ?  AND " +
                        "branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"
                )
        );
    }

    @ParameterizedTest
    @MethodSource("args")
    void testSort(Integer id, Integer lim, Integer sin, Boolean desc, String query) {
        ThreadDAO.treeSort(id, lim, sin, desc);
        Mockito.verify(jdbcMock).query(
                Mockito.eq(query),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.any()
        );
    }
}