package com.hw.db.DAO;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;

class ForumDAOTests {
    JdbcTemplate jdbcMock;
    ForumDAO forumMock;

    @BeforeEach
    void init() {
        jdbcMock = Mockito.mock(JdbcTemplate.class);
        forumMock = new ForumDAO(jdbcMock);
    }

    private static List<Arguments> args() {
        return List.of(
                Arguments.of(
                        "slug", null, null, false,
                        "SELECT nickname,fullname,email,about FROM forum_users " +
                        "WHERE forum = (?)::citext ORDER BY nickname;"
                ),
                Arguments.of(
                        "slug", null, null, true,
                        "SELECT nickname,fullname,email,about FROM forum_users " +
                        "WHERE forum = (?)::citext ORDER BY nickname desc;"
                ),
                Arguments.of(
                        "slug", 1, null, false,
                        "SELECT nickname,fullname,email,about FROM forum_users " +
                        "WHERE forum = (?)::citext ORDER BY nickname LIMIT ?;"
                ),
                Arguments.of(
                        "slug", 1, null, true,
                        "SELECT nickname,fullname,email,about FROM forum_users " +
                        "WHERE forum = (?)::citext ORDER BY nickname desc LIMIT ?;"
                ),
                Arguments.of(
                        "slug", null, "test", false,
                        "SELECT nickname,fullname,email,about FROM forum_users " +
                        "WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"
                ),
                Arguments.of(
                        "slug", null, "test", true,
                        "SELECT nickname,fullname,email,about FROM forum_users " +
                        "WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;"
                ),
                Arguments.of(
                        "slug", 1, "test", true,
                        "SELECT nickname,fullname,email,about FROM forum_users " +
                        "WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"
                )
        );
    }

    @ParameterizedTest
    @MethodSource("args")
    void testUsers(String slug, Integer lim, String since, Boolean desc, String query) {
        ForumDAO.UserList(slug, lim, since, desc);
        Mockito.verify(jdbcMock).query(
                Mockito.eq(query),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class)
        );
    }
}